// document.write("hello");
// var namePrimitive = "Zeinab Muhammad Aamer";
// console.log(name);

// var firstName = new String("Zeinab Muhammad Aamer");
// firstName.charAt(0)
// firstName[0] = firstName[0].toLowerCase()
// firstName.length
// firstName.split("a")
/*********** loop *********** */
// console.log("my name");
// console.log("my name");
// console.log("my name");
// console.log("my name");
// console.log("my name");

// for(initialValue; condition; counterExpression){

// }


// let number = +prompt("enter a number"," ")
// var fact= 1;
// for(i =1;i<=number;i++){
//     fact*=i;
    
// }
// console.log(fact);

// let person = {
//     name: "Ahmed",
//     age: 29,
//     job : "Java developer"
// }
// for(key in person){
//     console.log(key, person["name"]);
// }

/** for .. of */
// const cars = ["volvo","lancer","kia"];

// console.log(cars);
// console.log(cars[0]);
// for(car of cars){
//     console.log(car);
// }
/****************** function and scope ***************** */
// var global = 10;
// function sum(x,y) {
//     global =20;
//     return x+y;
// } // terminate
// console.log("before" +global);

// console.log(sum(2,3));

/************** object********** */ 
// const circle ={
//     radius: 5,
//     location: {
//         x:1,
//         y:2
//     },
//     isVisible: true,
//     draw: function(){
//         console.log("draw function");
//     }
// }
// console.log(circle);
/***************  factory function (camel case) ****************/
// camel Case
// function createCircle (radius,x,y){
//     return {
//         radius,
//         x,
//         y,
//         draw(){
//             console.log("draw function");
//         }
//     }
// }

// const cirle1 = new createCircle(3,2,3);
// console.log (cirle1);

// const circle2 =createCircle(5,8,9);
// console.log (circle2);

/* *************** constructor (Pascal Case)***************** */
function Circle(radius){
    this.radius = radius;
    this.draw = function(){
        console.log("draw from constructor function");
    }
}

const circle3 = new Circle(5);
console.log(circle3);

